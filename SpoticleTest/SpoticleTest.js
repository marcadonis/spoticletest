var id = 0;
var comments = [ //
{
	id : id++,
	date : new Date(2015, 0, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
},

{
	id : id++,
	date : new Date(2015, 1, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
}, {
	id : id++,
	date : new Date(2015, 2, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
}, {
	id : id++,
	date : new Date(2015, 3, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
}, {
	id : id++,
	date : new Date(2015, 4, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
}, {
	id : id++,
	date : new Date(2015, 5, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
},

{
	id : id++,
	date : new Date(2015, 6, 1, 0, 0, 0, 0),
	votes : {
		up : 22,
		down : 4
	},
	lastActivity : new Date(2015, 0, 1, 0, 0, 0, 0),
} //
];

var VOTE_WEIGHT = 2000;
var LAST_ACTIVITY_TIME_WEIGHT = 500;

if (Meteor.isClient) {

	Session.set("comments", comments);

	Template.body.helpers({
		comments : function() {
			return Session.get("comments");
		},

		age : function() {
			Session.get("refresh");
			var m = moment(this.date);
			return m.fromNow();
		},

		ageLastActivity : function() {
			Session.get("refresh");
			var m = moment(this.lastActivity);
			return m.fromNow();
		},

		voteWeight : function() {
			return VOTE_WEIGHT;
		},

		lastActivityTimeWeight : function() {
			return LAST_ACTIVITY_TIME_WEIGHT;
		}
	});

	Template.body.events({
		"click #sort-button" : function(event) {
			VOTE_WEIGHT = $("#vote-weight-value").val();
			LAST_ACTIVITY_TIME_WEIGHT = $("#last-activity-time-weight-value").val();
			sortByRelevance();
		},

		"click .create-activity-button" : function(event) {
			findComment(this.id).lastActivity = new Date();
			sortByRelevance();
		},

		"blur .up-votes" : function(event) {
			findComment(this.id).votes.up = parseInt(event.target.value);
			sortByRelevance();
		},

		"blur .down-votes" : function(event) {
			findComment(this.id).votes.down = parseInt(event.target.value);
			sortByRelevance();
		}
	});
}

function sortByRelevance() {
	var now = new Date().getTime();
	comments.sort(function(comment1, comment2) {
		var ageComment1 = now - comment1.date.getTime();
		var ageComment2 = now - comment2.date.getTime();

		var lastActivityComment1 = 0;
		var lastActivityComment2 = 0;
		if (comment1.lastActivity && comment2.lastActivity) {
			lastActivityComment1 = now - comment1.lastActivity.getTime();
			lastActivityComment2 = now - comment2.lastActivity.getTime();
		}

		var numUpvotesComment1 = comment1.votes.up;
		var numUpvotesComment2 = comment2.votes.up;
		var numDownvotesComment1 = comment1.votes.down;
		var numDownvotesComment2 = comment2.votes.down;
		var netVotesComment1 = numUpvotesComment1 - numDownvotesComment1;
		var netVotesComment2 = numUpvotesComment2 - numDownvotesComment2;

		//		var distanceComment1 = distance(center, comment1.location.coordinates);
		//		var distanceComment2 = distance(center, comment2.location.coordinates);
		var result = //
		(ageComment1 - ageComment2) + //
		VOTE_WEIGHT * (netVotesComment2 - netVotesComment1) + //
		LAST_ACTIVITY_TIME_WEIGHT * (lastActivityComment1 - lastActivityComment2)
		//		- DISTANCE_WEIGHT * (distanceComment2 - distanceComment1);
		return result;
	});

	Session.set("comments", comments);
	Session.set("refresh", Math.random());
}

function findComment(id) {
	for (var i = 0; i < comments.length; i++) {
		var comment = comments[i];
		if (comment.id == id) {
			return comment;
		}
	}
	return null;
}
